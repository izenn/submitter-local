#!/usr/bin/python3
import json
import re
import os
import shutil
import base64
import pprint
  
f = open('post.json')
  
data = json.load(f)
  
output_dir = "uploads/" + data['info']['title'] + "-" + data['info']['version']
build_dir = "build/" + data['info']['title']
shutil.rmtree(output_dir, ignore_errors = True)
shutil.rmtree(build_dir, ignore_errors = True)
os.makedirs(output_dir, exist_ok = True)
os.makedirs(build_dir, exist_ok = True)

for i in range(len(data['assets'])):
  if (data['assets'][i]['type'] == "screenshot"):
      filename = "screen.png"
  elif (data['assets'][i]['type'] == "icon"):
      filename = "icon.png"
  elif (data['assets'][i]['type'] == "zip"):
      filename = "package.zip"
  else:
      print("unknown asset")
      print(data['assets'][i]['type'])
      raise SystemExit

  encoded_string = re.split(r'[,:;]', data['assets'][i]['data'])
  decoded_data=base64.b64decode(encoded_string[3])
  posted_file = open(output_dir + "/" + filename, 'wb')
  posted_file.write(decoded_data)
  posted_file.close()

  del data['assets'][i]['data']
  del data['assets'][i]['format']

  data['assets'][i]['url'] = "../../" + output_dir + "/" + filename

  if (data['assets'][i]['type'] == "zip"):
      zip_asset = {"zip": [{"path": "/**", "dest": "/", "type": "update"}]}
      data['assets'][i].update(zip_asset)

pprint.pprint(data)

json.dump(data, open(build_dir + "/pkgbuild.json", "w"), indent=4)

f.close()

os.remove("post.json")
