## Submitter
Adapted to be able to host a local repository when using spinarak.  Made to be used in the hbas-local docker image.  if you are going to run it stand-alone, change the word CHANGEME in index.html and status.html to your IP address.

This fork does not use dragonite, and once the package is submitted, it will process the upload, publish it to a repository and redirect you to the hbas-frontend.

### Original Text
Submit apps to our homebrew repos! This form directly posts to [Dragonite](http://gitlab.com/4tu/dragonite), which pending approval makes its way to [Spinarak](http://gitlab.com/4tu/spinarak).

### Our Metadata Repos
- [switch.apps](http://gitlab.com/4tu/switch.apps) - Homebrew apps for Switch
- [wiiu.apps](http://gitlab.com/4tu/wiiu.apps) - Homebrew apps for Wii U
- [switch.themes](http://gitlab.com/4tu/switch.themes) - System themes for Switch
- [wiiu.plugins](http://gitlab.com/4tu/wiiu.plugins) - [WUPS](https://github.com/Maschell/WiiUPluginSystem) plugins for Wii U

The information in our metadata repos is available under a [CC-BY-SA](https://creativecommons.org/licenses/by-sa/4.0/legalcode) license.
